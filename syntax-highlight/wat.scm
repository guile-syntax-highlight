;;; guile-syntax-highlight --- General-purpose syntax highlighter
;;; Copyright © 2023 David Thompson <davet@gnu.org>
;;;
;;; Guile-syntax-highlight is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 3 of the License, or (at your option) any later version.
;;;
;;; Guile-syntax-highlight is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-syntax-highlight.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Syntax highlighting for WebAssembly text format.
;;
;;; Code:

(define-module (syntax-highlight wat)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (syntax-highlight lexers)
  #:export (%default-special-symbols
            %default-special-prefixes
            make-wat-lexer
            lex-wat))

(define char-set:wat-delimiters
  (char-set-union char-set:whitespace
                  (char-set #\( #\))))

(define char-set:wat-symbol
  (char-set-complement char-set:wat-delimiters))

(define %keywords
  '("i31" "i32" "i64" "f32" "f64" "eq" "string" "module" "type" "func"
    "array" "struct" "mut" "ref" "extern" "null" "field" "sub" "import"
    "export" "param" "result" "block" "loop" "if" "then" "else" "local"
    "rec" "memory" "data" "table" "elem" "global" "start"))

;; TODO: WAT has some weird commenting syntax that this doesn't
;; support, among probably other missing things.
(define lex-wat
  (lex-consume
   (lex-any (lex-char-set char-set:whitespace)
            (lex-tag 'open (lex-string "("))
            (lex-tag 'close (lex-string ")"))
            (lex-tag 'comment (lex-delimited ";" #:until "\n"))
            (lex-tag 'special
                     (lex-filter (lambda (str)
                                   (any (cut string=? <> str) %keywords))
                                 (lex-char-set char-set:wat-symbol)))
            (lex-tag 'string (lex-delimited "\""))
            (lex-tag 'symbol (lex-char-set char-set:wat-symbol)))))
